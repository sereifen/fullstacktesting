package Tester.SQL;

import Tester.Data.Band;
import Tester.Data.Bands;

import java.sql.*;
import java.util.ArrayList;

public class Thrower {

    private Connection _conn;

    private static String url = "ec2-54-93-206-180.eu-central-1.compute.amazonaws.com";

    public Thrower()
    {
        _conn = GetConnection();
    }

    private static Connection GetConnection() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            return DriverManager.getConnection("jdbc:mysql://"+url+"/metal?"
                    + "user=jeffrey&password=some_pass");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Bands GetBands() throws SQLException {
        Bands bands =  new Bands();
        if (_conn == null) return null;
        Statement statement = _conn.createStatement();
        ResultSet res = statement.executeQuery("select * from metal.bands");
        while (res.next())
        {
            Band aux = new Band();
            aux.setName(res.getString("name"));
            aux.setEnd(res.getString("end"));
            aux.setLocation(res.getString("location"));
            aux.setStart(res.getString("start"));
            aux.setType(res.getString("type"));
            bands.bands.add(aux);
        }
        return bands;
    }
}
