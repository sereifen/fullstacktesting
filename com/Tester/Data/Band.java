package Tester.Data;


public class Band {
    private String name;
    private String type;
    private String start;
    private String end;
    private String location;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @Override
    public boolean equals(Object other){
        if (other == null) return false;
        if (other == this) return true;
        if (!(other instanceof Band))return false;
        Band aux = (Band)other;
        if (aux.getEnd().equals(end) && aux.getLocation().equals(location) && aux.getName().equals(name) &&
                aux.getType().equals(type) && aux.getStart().equals(start))
            return true;
        return false;
    }
}
