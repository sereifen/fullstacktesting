package Tester.API;

import Tester.Data.Bands;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class APIConnection {

    private static String urlAPI = "http://ec2-54-93-206-180.eu-central-1.compute.amazonaws.com:8080/ErniTest/Metal/json";

    public static Bands GetValueAPI() throws IOException {
        StringBuilder result = new StringBuilder();
        URL url = new URL(urlAPI);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String line;
        result.append("{\"bands\" :");
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }
        result.append("}");
        rd.close();

        return new Gson().fromJson(result.toString(), Bands.class);
    }
}
