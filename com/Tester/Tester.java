package Tester;

import Tester.API.APIConnection;
import Tester.Data.Band;
import Tester.Data.Bands;
import Tester.SQL.Thrower;
import Tester.Web.WebTester;
import com.google.gson.Gson;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;

public class Tester {

    private Thrower _conn = null;
    private static String _filename = "test.json"; //TODO need change
    private String _messageOK = "";
    private String _messageKO = "";

    public Tester ()
    {
        _conn = new Thrower();
    }

    private void RunTestSQLtoAPI() {
        Bands server = null;
        Bands api = null;
        try {
            //get values from DB
            server = _conn.GetBands();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            _messageKO +="fail receiving data from DB\n";
        }
        try
        {
            api = APIConnection.GetValueAPI();
        }
        catch (IOException e)
        {
            e.printStackTrace();
            _messageKO +="fail receiving data from API\n";
        }

        if (CompareBands(api,server))
        {
            _messageOK += "The Test Database Vs API works and result was OK\n\n";
        }
        else
        {
            _messageKO += "The Test Database Vs API works but result was KO\n\n";
        }
    }

    private boolean CompareBands(Bands api, Bands server) {
        //TODO this is ur work ;)
        return false;
    }

    private void RunTestSQLtoWeb() {
        Bands server = null;
        Bands web = null;
        try {
            //get values from DB
            server = _conn.GetBands();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            _messageKO +="fail receiving data from DB\n";
        }
        try
        {//TODO need change
            WebTester tester = new WebTester("C:\\Dev\\SEALS_SOFT\\tools\\AutoTestServer\\Selenium\\Drivers\\chromedriver_win32\\chromedriver.exe","webdriver.chrome.driver");
            web = tester.getBands();
        }
        catch (Exception e)
        {
            e.printStackTrace();
            _messageKO +="fail receiving data from API\n";
        }

        if (CompareBands(web,server))
        {
            _messageOK += "The result of compare if the Database Grow works and the result is OK\n\n";
        }
        else
        {
            _messageKO += "The result of compare if the Database Grow works and the result is KO\n\n";
        }
    }

    private void RunTestDBGrow() {
        Bands server = null;
        Bands file;
        try {
            //get values from DB
            server = _conn.GetBands();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            _messageKO +="fail receiving data from DB\n";
        }
        //get values from file
        file = GetBandFromFile(_filename);
        //check match
        if (CompareBandsFile(file,server))
        {
            WriteFile(_filename, server);
            _messageOK += "The result of compare if the Database Grow works and the result is OK\n\n";
        }
        else
        {
            _messageKO += "The result of compare if the Database Grow works and the result is KO\n\n";
        }
    }

    private boolean CompareBandsFile(Bands file, Bands server) {
        //TODO this is ur work ;)
        return false;
    }


    private void WriteFile(String filename, Bands server) {
        try {
            PrintWriter writer = new PrintWriter(filename, "UTF-8");
            writer.println(new Gson().toJson(server));
            writer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }



    private Bands GetBandFromFile(String file) {
        BufferedReader br = null;
        FileReader fr = null;
        Bands result = null;
        File f = new File(file);
        if(!f.exists() || f.isDirectory())
            return null;
        try {

            //br = new BufferedReader(new FileReader(FILENAME));
            fr = new FileReader(file);
            br = new BufferedReader(fr);

            String sCurrentLine, buffer = "";

            while ((sCurrentLine = br.readLine()) != null) {
                buffer += sCurrentLine;
                System.out.println(sCurrentLine);
            }
            result = new Gson().fromJson(buffer.toString(), Bands.class);

        } catch (IOException e) {
            e.printStackTrace();
            _messageKO += "fail while reading file\n";
        } finally {
            try {
                if (br != null)
                    br.close();
                if (fr != null)
                    fr.close();
            } catch (IOException ex) {
                _messageKO += "fail while closing file\n";
                ex.printStackTrace();
            }
        }
        return result;
    }

    private void SendEmail() {
        String from = "fullstacktesting@outlook.es";
        String to = "fullstacktesting@outlook.es";
        //String to = "erni-services.joan.calmet@hp.com";
        System.out.println("Send Email to" + to);
        Properties props = new Properties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.host", "smtp-mail.outlook.com");
        props.put("mail.smtp.port", "587");
        props.put("mail.smtp.timeout", "10000");
        props.put("mail.smtp.connectiontimeout", "10000");
        props.put("mail.smtp.debug", "true");
        props.put("mail.smtp.socketFactory.fallback", "false");
        props.put("mail.smtp.starttls.enable", "true");
        Session session = Session.getInstance(props,
                new Authenticator() {
                    @Override
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication("fullstacktesting@outlook.es", "escroto_moreno");
                    }
                });
        session.setDebug(true);
        try
        {
            MimeMessage message = new MimeMessage(session);
            message.setSentDate(new Date());
            message.setFrom(new InternetAddress(from));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
            message.setSubject("Web Testing " + (_messageKO == "" ? "All works" : "Miss match on application"));
            message.setText(_messageKO == "" ? _messageOK : _messageKO + "\n\n\n\n" + _messageOK);
            Transport.send(message);
        }
        catch (Exception mex)
        {
            mex.printStackTrace();
        }
    }

    public void RullAllTest() {
        RunTestDBGrow();
        RunTestSQLtoAPI();
        RunTestSQLtoWeb();
        SendEmail();
    }
}


















































































































/*private boolean CompareBands(Bands file, Bands server) {
        if (file == null && server != null) return false;
        if (file != null && server == null) return false;
        if (file == null && server == null) return true;
        if (file.bands.size() != server.bands.size()) return false;
        for (Band bfile: file.bands){
            boolean find = false;
            for (Band bserver: server.bands) {
                if (bfile.equals(bserver)) {
                    find = true;
                    break;
                }
            }
            if (!find) return false;
        }
        return true;
    }*/














/*
private boolean CompareBandsFile(Bands file, Bands server) {
        if (file == null && server != null) return true;
        if (file != null && server == null) return false;
        if (file == null && server == null) return true;
        if (file.bands.size() > server.bands.size()) return false;
        for (Band bfile: file.bands){
            boolean find = false;
            for (Band bserver: server.bands) {
                if (bfile.equals(bserver)) {
                    find = true;
                    break;
                }
            }
            if (!find) return false;
        }
        return true;
    }
 */